package im.tester.common.uitls;

public class Constant {
    public static enum RpcRespCode {
        success(1),
        fail(-1),

        appium_not_start(-100),
        device_not_connect(-200),
        docker_not_read(-300),

        host_not_read(-400),
        kvm_not_read(-500);

        RpcRespCode(Integer val) {
            this.val = val;
        }

        private Integer val;
        public Integer getCode() {
            return val;
        }
    }
}
